# mst-netgroup

[[_TOC_]]

## Installation

### Production Installation

Command Line:

```bash
pip install mst-netgroup --index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
```

requirements.txt:

```text
--extra-index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
mst-netgroup
```

## Usage

```python
from mst.netgroup import Netgroup

# Login is a class method, so the login state is shared between all instances
Netgroup.login(username="username", password="password")

test_group = Netgroup("it-temp-testing")
it_staff = Netgroup("it-staff")

# Print name
print(test_group)

# Print members
print(test_group.members)

# Membership is cached to reduce network calls (LRU cache with ttl=60).
# The add_member and remove_member functions will automatically clear the cache.
# To manually clear the cache:
test_group._members.cache_clear()

# Print number of members
print(len(test_group))

# Return true if group exists
print(bool(test_group))

# Iterate over membership
for member: str in test_group:
    print(member)

# Has support for most basic set operations
# Other set operations can be accessed via .members if needed
print("OR/UNION", test_group | it_staff)
print("AND/INTERSECTION", test_group & it_staff)
print("XOR/SYMMETRIC DIFFERENCE", test_group ^ it_staff)
print("DIFFERENCE", it_staff - test_group)


# Remove member from netgroup
test_group.remove_member("joeminer")

# Add member to netgroup
test_group.add_member("joeminer")

# Checking Netgroup('group1') == Netgroup('group2') will return true if the names are the same
print("Same Group/Name", test_group == it_staff)

# Check if two groups have the same membership
print("Same members", test_group.members == it_staff.members)

# Check if user is in group
print("joeminer" in test_group)
```
